<?php

/*
Lines required for GeoIP
require_once $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
use GeoIp2\Database\Reader;
*/

class Testmodule_Currency_Model_Store extends Mage_Core_Model_Store
{
    

    public function getDefaultCurrencyCode()
    {

        // This creates the Reader object, which should be reused across
        // lookups.
        $reader = new Reader($_SERVER['DOCUMENT_ROOT'] . '/geoip/GeoLite2-Country_20171205/GeoLite2-Country.mmdb');
        $record = $reader->country($_SERVER['REMOTE_ADDR']);
        
        // by default we set the required currency code
        if (isset($_SERVER['HTTP_USER_AGENT'])
            && preg_match('/bot|crawl|google|slurp|spider|mediapartners/i', $_SERVER['HTTP_USER_AGENT'])) {
            $result = "GBP";
            return $result;
        }

        $result = $this->getConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_DEFAULT);
        // however, in our case we want to determine the default currency depending on the country/continent of the visitor
        $geoCountryCode = null;
        try {
            // we'll try to get the visitor country
            $geoCountryCode = $record->country->isoCode;
            /*
                Old code that does work
                $geoCountryCode = $_SERVER["GEOIP_COUNTRY_CODE"] ;
            */

        } catch (Exception $e) {
            // prevent NOTICE error - for example we are running the code on a localhost
        }
        
        // first tier check is the specific countries to set the currency for
        // NOTE: you can roll your own logic here depending on your enabled/supported countries/currencies
        // this example assumes that AUD, GBP, JPY, USD, EUR and NZD are the supported currencies 
        switch ($geoCountryCode) {
            case "GB":
                $result = "GBP";
                break;
            case "DK":
                $result = "DKK";
                break;
            default:
                $geoContinentCode = null;
                // now grab the continent code and set further by general regions
                try {
                    $geoContinentCode = $_SERVER["GEOIP_CONTINENT_CODE"];
                    // You can debug here to check the continent codes...
                    //Mage::log("GEOIP::CONTINENT::".$geoCountryCode);
                } catch (Exception $e) {
                    //Mage::log("EXCEPTION CAUGHT".$e->getMessage());
                    // prevent NOTICE error
                }
                // Now decide what currency to set depending on broad regions
                switch ($geoContinentCode) {
                    case "EU": // we'll set EUR for European countries
                        $result = "EUR";
                        break;
                    case "NA": // North America
                    case "SA": // South America
                    case "AS": // Asia
                    case "AF": // Africa - all of them will see USD
                        $result = "GBP";
                        break;
                    default:    // everything else uses the default store currency as set in config
                }
        }
        return $result;

    }
}
